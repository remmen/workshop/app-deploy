## Structure

The deployment is based on a classical kustomize structure with a base and overlays for each environment. The argocd folder contains the application definition.
```
├── README.md
└── kustomize
    ├── base
    │   ├── deployment.yml
    │   ├── kustomization.yml
    │   └── service.yml
    └── overlay
        └── main
            ├── ingress.yml
            └── kustomization.yml
